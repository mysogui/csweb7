<?php
define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', '');
define('DBNAME', 'dbcsweb');
define('ENABLE_OAUTH', true);
define('FILES_FOLDER', 'C:\wamp64\www\csweb\files');
define('DEFAULT_TIMEZONE', 'UTC');
define('MAX_EXECUTION_TIME', '300');
define('API_URL', 'http://localhost/csweb/api/');
define('CSWEB_LOG_LEVEL' , 'error');
define('CSWEB_PROCESS_CASES_LOG_LEVEL', 'error');
?>
